package com.kfpiwebstandard.backing;

import java.io.Serializable;
import javax.inject.Named;
import com.kfpiwebstandard.gpg3controller.GoPiGo3;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.enterprise.context.SessionScoped;

/**
 * Backing bean for index.xhtml This class sends messages to the GoPiGo3
 * controller class
 *
 * @author Ken Fogel
 * @version 0.1
 */
@Named("gopigoBacking")
@SessionScoped
public class GoPiGoBacking implements Serializable {

    static final Logger LOG = Logger.getLogger(GoPiGoBacking.class.getName());

    private final GoPiGo3 gopigo;

    /**
     * Constructor initializes GoPiGo3 object
     *
     * @throws Exception
     */
    public GoPiGoBacking() throws Exception {
        gopigo = new GoPiGo3();
    }

    /**
     * Forward
     *
     * @throws IOException
     */
    public void goForward() throws IOException {
        LOG.log(Level.INFO, "Moving forward");
        gopigo.runMotor(GoPiGo3.MOTOR_LEFT, 100);
        gopigo.runMotor(GoPiGo3.MOTOR_RIGHT, 100);
    }

    /**
     * Reverse
     *
     * @throws IOException
     */
    public void goReverse() throws IOException {
        LOG.log(Level.INFO, "Moving reverse");
        gopigo.runMotor(GoPiGo3.MOTOR_LEFT, -100);
        gopigo.runMotor(GoPiGo3.MOTOR_RIGHT, -100);
    }

    /**
     * Left
     *
     * @throws IOException
     */
    public void goLeft() throws IOException {
        LOG.log(Level.INFO, "Moving left");
        gopigo.stopMotor(GoPiGo3.MOTOR_LEFT);
        gopigo.runMotor(GoPiGo3.MOTOR_RIGHT, 100);
    }

    /**
     * Right
     *
     * @throws IOException
     */
    public void goRight() throws IOException {
        LOG.log(Level.INFO, "Moving right");
        gopigo.stopMotor(GoPiGo3.MOTOR_RIGHT);
        gopigo.runMotor(GoPiGo3.MOTOR_LEFT, 100);
    }

    /**
     * Stop
     *
     * @throws IOException
     */
    public void goStop() throws IOException {
        LOG.log(Level.INFO, "Stopping");
        gopigo.stopMotors();
    }
}
