This project is a proof of concept for controlling a GoPiGo3 robot car using the Payara Micro server. It is a basic JavaServer Faces application with 5 buttons that control the motors on the GoPiGo3 to drive it forward, left, right, reverse or to stop the car.
You can read my blog post about this code at https://www.omnijava.com/2018/05/27/raspberry-pi-jav…proof-of-concept/.


